const gulp = require("gulp");
const browserify = require("browserify");
const tsify = require("tsify");
const buffer = require("vinyl-buffer");
const sourcemaps = require("gulp-sourcemaps");
const source = require("vinyl-source-stream");
const server = require("gulp-webserver");
const postcss = require("gulp-postcss");
const tailwindcss = require("tailwindcss");
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");

gulp.task("compile-js", () => {
  return browserify({
    basedir: ".",
    debug: true,
    entries: ["src/index.tsx"],
    cache: {},
    packageCache: {},
    insertGlobals: true,
  })
    .plugin(tsify, { transpileOnly: true })
    .transform("babelify", {
      presets: ["@babel/env", "@babel/react"],
      plugins: ["@babel/plugin-transform-runtime"],
      compact: true,
      extensions: [".tsx"],
    })
    .bundle()
    .pipe(source("bundle.js"))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadmaps: true }))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("dist"));
});

gulp.task("copy-html", () => {
  return gulp.src("src/index.html").pipe(gulp.dest("dist"));
});

gulp.task("process-css", () => {
  const plugins = [tailwindcss, cssnano, autoprefixer({ cascade: false })];
  return gulp.src("src/*.css").pipe(postcss(plugins)).pipe(gulp.dest("dist"));
});

gulp.task("webserver", () => {
  gulp.src("dist").pipe(
    server({
      livereload: true,
      open: true,
      port: 3000,
      fallback: "/index.html",
    })
  );
});

gulp.task("default", gulp.series("copy-html", "compile-js", "process-css"));
