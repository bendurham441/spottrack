import cryptoRandomString from "crypto-random-string";

class Authenticator {
  token: string;
  refreshToken: string;
  expiry: number;
  clientId: string;

  getToken() {
    if (new Date().getTime() / 1000 >= this.expiry) {
      return "need to refresh";
    }
    return this.token;
  }

  async refresh() {
    let response = await fetch("https://accounts.spotify.com/api/token", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: JSON.stringify({
        grant_type: "refresh_token",
        refresh_token: this.refreshToken,
        client_id: "application/x-www-form-urlencoded",
      }),
    });
    let data = response.json();
    console.log(data);
  }

  static async getCode() {
    let verifier = cryptoRandomString({ length: 50 });
    window.localStorage.setItem("verifier", verifier);

    async function sha256(plain: string) {
      const encoder = new TextEncoder();
      const data = encoder.encode(plain);

      return window.crypto.subtle.digest("SHA-256", data);
    }

    function base64urlencode(a: ArrayBuffer) {
      return btoa(
        String.fromCharCode.apply(null, new Uint8Array(a as ArrayBuffer))
      )
        .replace(/\+/g, "-")
        .replace(/\//g, "_")
        .replace(/=+$/, "");
    }

    const hashed = await sha256(verifier);
    const challenge = base64urlencode(hashed);

    let params = {
      client_id: "c3ddcda637ea42808d3e8834383d3a70",
      response_type: "code",
      redirect_uri: "http://localhost:3000/callback",
      code_challenge_method: "S256",
      code_challenge: challenge,
      scope: "user-top-read"
    };

    let endpoint = new URL("https://accounts.spotify.com/authorize");
    endpoint.search = new URLSearchParams(params).toString();

    window.location.href = endpoint.toString();
  }

  async getInitialToken() {
    let params = new URLSearchParams(window.location.search);

    let newParams = {
      client_id: "c3ddcda637ea42808d3e8834383d3a70",
      grant_type: "authorization_code",
      code: params.get("code"),
      redirect_uri: "http://localhost:3000/callback",
      code_verifier: window.localStorage.getItem("verifier"),
    };

    let endpoint = new URL("https://accounts.spotify.com/api/token");

    endpoint.search = new URLSearchParams(newParams).toString();

    let response = await fetch(endpoint.toString(), {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    });
    let data = await response.json();
    this.expiry = Math.floor(new Date().getTime() / 1000) + data.expires_in;
    this.token = data.access_token;
    this.refreshToken = data.refresh_token;
    return data.access_token;
  }
}

export { Authenticator };
