import * as React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import Home from "./Home";
import Callback from "./Callback";
import Heading from "./common/Heading";
import Footer from "./common/Footer";
import TopArtists from "./TopArtists";

const App = () => {
  return (
    <Router>
      <div id="page-container">
        <div id="content-wrap" className="container sm:w-full md:w-3/5 mx-auto">
          <Heading />
          <Switch>
            <Route path="/callback">
              <Callback />
            </Route>
            <Route path="/">
              <Home />
            </Route>
            <Route path="/top-artists">
            </Route>
          </Switch>
        </div>
        <Footer />
      </div>
    </Router>
  );
};

export default App;
