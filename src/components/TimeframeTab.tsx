import * as React from "react";
import { TimeFrame } from "./TopArtists";

interface TimeframeProps {
  text: string;
  selected: boolean;
  setTimeFrame: Function;
  setLoading: Function;
}

const TimeframeTab = ({ text, selected, setTimeFrame, setLoading }: TimeframeProps) => {
  let classes = "text-center inline-block w-1/3 p-3";
  if (selected) {
    classes +=
      " bg-gray-100 text-purple-600 border-t-2 border-purple-600 shadow-md";
  } else {
    classes += " bg-gray-400 text-gray-600";
  }
  return (
    <div
      className={classes}
      onClick={() => {
        if (!selected) {
          setLoading(true);
          switch (text) {
            case "4 Weeks":
              setTimeFrame(TimeFrame.Short);
              break;
            case "6 Months":
              setTimeFrame(TimeFrame.Medium);
              break;
            case "All Time":
              setTimeFrame(TimeFrame.Long);
              break;
          }
        }
      }}
    >
      {text}
    </div>
  );
};

export default TimeframeTab;
