import * as React from "react";
import { Link } from "react-router-dom";

import { Authenticator } from "../utils/auth";

import Heading from "./common/Heading";
import Footer from "./common/Footer";

const Home = () => {
  return (
    <div>
      <h2 className="text-center text-gray-600 text-3xl">
        Analyze your Spotify data
      </h2>
      <div className="flex justify-center my-16">
        <button
          className="p-4 bg-indigo-700 text-white rounded"
          onClick={Authenticator.getCode}
        >
          View your data
        </button>
      </div>
      <div className="w-3/5 mx-auto">
        <p>
          Spotstat utilizes the Spotify Web API to show your Spotify data such
          as top tracks, artists, and other stats.
        </p>
      </div>
    </div>
  );
};

export default Home;
