import * as React from "react";

import { Authenticator } from "../utils/auth";

import TopSongs from "./TopArtists";

const Callback = () => {
  let [tokenGenerated, setTokenGenerated] = React.useState(false);
  let [auth, setAuth] = React.useState(new Authenticator());

  React.useEffect(() => {
    const authenticate = async () => {
      await auth.getInitialToken();
      setTokenGenerated(true);
    };
    authenticate();
  }, []);

  if (tokenGenerated) {
    return (
      <div>
        <TopSongs auth={auth} />
      </div>
    );
  }
  return <p>Loading...</p>;
};

export default Callback;
