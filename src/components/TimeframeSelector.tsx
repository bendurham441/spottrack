import * as React from "react";

import TimeframeTab from "./TimeframeTab";
import { Authenticator } from "../utils/auth";
import { TimeFrame } from "./TopArtists";

interface TimeFrameSelectorProps {
  setData: Function;
  auth: Authenticator;
  timeFrame: TimeFrame;
  setTimeFrame: Function;
  setLoading: Function;
}

const TimeframeSelector = ({
  setData,
  auth,
  timeFrame,
  setTimeFrame,
  setLoading
}: TimeFrameSelectorProps) => {
  return (
    <div className="my-3">
      <TimeframeTab
        setTimeFrame={setTimeFrame}
        selected={timeFrame === TimeFrame.Short}
        text="4 Weeks"
        setLoading={setLoading}
      />
      <TimeframeTab
        setTimeFrame={setTimeFrame}
        selected={timeFrame === TimeFrame.Medium}
        text="6 Months"
        setLoading={setLoading}
      />
      <TimeframeTab
        setTimeFrame={setTimeFrame}
        selected={timeFrame === TimeFrame.Long}
        text="All Time"
        setLoading={setLoading}
      />
    </div>
  );
};

export default TimeframeSelector;
