import * as React from "react";
import { Authenticator } from "../utils/auth";

import Card from "./common/Card";
import TimeframeSelector from "./TimeframeSelector";

type TopSongsProps = {
  auth: Authenticator;
};

interface Response {
  items: Object[];
}

interface Artist {
  name: string;
  artist: string[];
  images: Image[];
}

interface Image {
  url: string;
}

enum TimeFrame {
  Short = "short_term",
  Medium = "medium_term",
  Long = "long_term",
}

const TopSongs = ({ auth }: TopSongsProps) => {
  let [data, setData] = React.useState<Response>();
  let [timeFrame, setTimeFrame] = React.useState(TimeFrame.Short);
  let [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    const getData = async () => {
      let url = new URL("https://api.spotify.com/v1/me/top/artists");
      url.search = new URLSearchParams({
        time_range: timeFrame,
      }).toString();

      let response = await fetch(url.toString(), {
        method: "GET",
        headers: {
          Authorization: `Bearer ${auth.getToken()}`,
        },
      });

      let responseData = await response.json();
      setData(responseData);
      setLoading(false);
    };
    getData();
  }, [timeFrame]);

  if (!loading) {
    return (
      <div className="sm:w-full md:w-1/2 mx-auto">
        <h1 className="text-center text-3xl my-8">Your Top Artists</h1>
        <TimeframeSelector
          timeFrame={timeFrame}
          setTimeFrame={setTimeFrame}
          auth={auth}
          setData={setData}
          setLoading={setLoading}
        />
        {data.items.map((item: Artist, i: number) => (
          <Card
            key={i}
            subText={null}
            mainText={item.name}
            image={item.images[0].url}
            index={i}
          />
        ))}
      </div>
    );
  }
  return (
    <div>
      <h2 className="text-4xl mx-auto">Loading...</h2>
    </div>
  );
};

export default TopSongs;
export { TimeFrame };
