import * as React from "react";

const Heading = () => {
  return (
    <h1 id="heading" className="text-center text-5xl my-4 text-purple-600">spottrack</h1>
  );
};

export default Heading;