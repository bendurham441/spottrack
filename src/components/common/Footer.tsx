import * as React from "react";

const Footer = () => {
  return (
    <div id="footer" className="w-full">
      <div className="flex justify-center">
        <div className="text-gray-500 p-4">About</div>
        <div className="text-gray-500 p-4">Terms and Conditions</div>
        <div className="text-gray-500 p-4">Privacy Policy</div>
      </div>
    </div>
  );
};

export default Footer;
