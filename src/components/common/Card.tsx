import * as React from "react";

interface CardProps {
  mainText: string;
  subText: string | null;
  index: number;
  image: string | null;
}

const Card = ({image, mainText, subText, index} : CardProps) => {
  return (
    <div className="flex mb-3 p-6 items-center shadow-md bg-gray-100 rounded">
      <div className="text-gray-600" style={{ width: 50 }}>
        <p className="text-center">{index + 1}</p>
      </div>
      <img
        className="mx-4"
        style={{ width: 80, height: 80, objectFit: "cover"}}
        src={image}
      />
      <p className="text-gray-800 flex-grow text-center">{mainText}</p>
    </div>
  );
};

export default Card;
